import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { MomentumComponent } from './components/momentum.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { AppGuard } from './guards/app.guard';
import { AuthInterceptor } from './interceptor/auth.interceptor';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { EventosComponent } from './components/eventos/eventos.component';
import { AddEventoComponent } from './components/eventos/add-evento/add-evento.component';
import { NgbModule, NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';
import { ViewEventoComponent } from './components/eventos/view-evento/view-evento.component';
import es from '@angular/common/locales/es-MX';
import { registerLocaleData } from '@angular/common';
import { CalendarEventosComponent } from './components/eventos/calendar-eventos/calendar-eventos.component';
import { ParticipantesComponent } from './components/participantes/participantes.component';
import { AddParticipanteComponent } from './components/participantes/add-participante/add-participante.component';
import { ViewParticipanteComponent } from './components/participantes/view-participante/view-participante.component';
import { MaterialModule } from './app-material.module';
import { AddParticipanteDialog } from './components/eventos/view-evento/add-participante-dialog/add-participante.dialog';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { NgxMaskModule } from 'ngx-mask'
import { AdminComponent } from './components/admin/admin.component';
import { TableTipoEventoComponent } from './components/admin/table-tipo-evento/table-tipo-evento.component';
import { AddTipoEventoDialog } from './components/admin/table-tipo-evento/add-tipo-evento-dialog/add-tipo-evento.dialog';
import { EditEventoComponent } from './components/eventos/edit-evento/edit-evento.component';
import { TableMetodoPagoComponent } from './components/admin/table-metodo-pago/table-metodo-pago.component';
import { AddMetodoPagoDialog } from './components/admin/table-metodo-pago/add-metodo-pago-dialog/add-metodo-pago.dialog';
import { PagosParticipanteDialog } from './components/eventos/view-evento/pagos-participante-dialog/pagos-participante.dialog';
import { NgxSpinnerModule } from "ngx-spinner";
import { ProximosEventosComponent } from './components/eventos/proximos-eventos/proximos-eventos.component';
import { ListaEventosComponent } from './components/eventos/lista-eventos/lista-eventos.component';
import { CardMovimientosEventoComponent } from './components/eventos/view-evento/card-movimientos-evento/card-movimientos-evento.component';
import { MovimientoEventoDialog } from './components/eventos/view-evento/movimiento-evento-dialog/movimiento-evento.dialog';
import { ParticipantesEnroladosDialog } from './components/participantes/participantes-enrolados-dialog/participantes-enrolados.dialog';
import { TableRolesComponent } from './components/admin/table-roles/table-roles.component';
import { MovimientosComponent } from './components/movimientos/movimientos.component';
import { CardListaMovimientosComponent } from './components/movimientos/card-lista-movimientos/card-lista-movimientos.component';
import { AddMovimientoDialog } from './components/movimientos/card-lista-movimientos/add-movimiento-dialog/add-movimiento.dialog';

registerLocaleData(es);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    MomentumComponent,
    SidebarComponent,
    EventosComponent,
    AddEventoComponent,
    ViewEventoComponent,
    CalendarEventosComponent,
    ParticipantesComponent,
    AddParticipanteComponent,
    ViewParticipanteComponent,
    AddParticipanteDialog,
    AddTipoEventoDialog,
    AdminComponent,
    TableTipoEventoComponent,
    EditEventoComponent,
    TableMetodoPagoComponent,
    AddMetodoPagoDialog,
    PagosParticipanteDialog,
    ProximosEventosComponent,
    ListaEventosComponent,
    CardMovimientosEventoComponent,
    MovimientoEventoDialog,
    ParticipantesEnroladosDialog,
    TableRolesComponent,
    MovimientosComponent,
    CardListaMovimientosComponent,
    AddMovimientoDialog
  ],
  imports: [
    NgbModule,
    NgbTypeaheadModule,
    ReactiveFormsModule,
    BrowserModule,
    MaterialModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    AutocompleteLibModule,
    NgxSpinnerModule,
    NgxMaskModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true
    })
  ],
  providers: [AppGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },{
      provide: LOCALE_ID,
      useValue: 'es-MX' }
  ],
  entryComponents: [
    AddParticipanteDialog,
    AddTipoEventoDialog,
    AddMetodoPagoDialog,
    PagosParticipanteDialog,
    MovimientoEventoDialog,
    ParticipantesEnroladosDialog,
    AddMovimientoDialog
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
