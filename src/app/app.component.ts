import { Component } from '@angular/core';
import { LoginService } from './services/login.service';
import { fadeAnimation } from './app-animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [fadeAnimation]
})
export class AppComponent {

  isUserInSession: boolean = false;

  constructor( private loginService: LoginService) {
    this.loginService.getUser$().subscribe(usuario => this.isUserInSession = usuario != null && usuario != undefined );
    this.isUserInSession = this.loginService.isUserInSession();
  }
}

