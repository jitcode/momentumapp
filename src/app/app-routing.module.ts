import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { AppGuard } from './guards/app.guard';
import { MomentumComponent } from './components/momentum.component';
import { EventosComponent } from './components/eventos/eventos.component';
import { AddEventoComponent } from './components/eventos/add-evento/add-evento.component';
import { ViewEventoComponent } from './components/eventos/view-evento/view-evento.component';
import { CalendarEventosComponent } from './components/eventos/calendar-eventos/calendar-eventos.component';
import { ParticipantesComponent } from './components/participantes/participantes.component';
import { AddParticipanteComponent } from './components/participantes/add-participante/add-participante.component';
import { ViewParticipanteComponent } from './components/participantes/view-participante/view-participante.component';
import { AdminComponent } from './components/admin/admin.component';
import { EditEventoComponent } from './components/eventos/edit-evento/edit-evento.component';
import { MovimientosComponent } from './components/movimientos/movimientos.component';

const routes: Routes = [
  { path: '', redirectTo: 'momentum/home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'momentum', component: MomentumComponent, canActivate: [AppGuard] ,
    children: [
      { path: '', redirectTo: 'momentum/home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },

      //Eventos
      { path: 'eventos', component: EventosComponent} ,
      { path: 'eventos/add', component: AddEventoComponent },
      { path: 'eventos/view/:id', component: ViewEventoComponent },
      { path: 'eventos/edit/:id', component: EditEventoComponent },
      { path: 'eventos/calendar', component: CalendarEventosComponent},

      //Participantes
      { path: 'participantes', component: ParticipantesComponent},
      { path: 'participantes/add', component: AddParticipanteComponent },
      { path: 'participantes/view/:id', component: ViewParticipanteComponent },

      //Admin
      { path: 'admin', component: AdminComponent},

      //Movimientos
      { path: 'movimientos', component: MovimientosComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
