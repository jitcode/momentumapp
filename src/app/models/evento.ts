import { TipoEvento } from './tipo-evento';

export class Evento {
  public id: number;
  public tipoEvento: TipoEvento = new TipoEvento();
  public generacion: string;
  public fechaInicio: Date;
  public fechaFin: Date;
  public descripcion: string;
}
