import { Participante } from './participante';
import { Evento } from './evento';

export class ParticipanteEvento {
  public participante: Participante;
  public evento: Evento;
  public costo: number;
  public asistio: boolean;
  public graduado: boolean;
}
