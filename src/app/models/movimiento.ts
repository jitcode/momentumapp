import { Evento } from './evento';
import { Participante } from './participante';
import { MetodoPago } from './metodo-pago';
import { TipoMovimiento } from './tipo-movimiento';

export class Movimiento {
  public id: number;
  public evento: Evento;
  public participante: Participante;
  public tipoMovimiento: TipoMovimiento;
  public metodoPago: MetodoPago;
  public descripcion: string;
  public fecha: Date;
  public cantidad: number;
}
