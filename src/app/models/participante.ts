export class Participante {
  public id: number;
  public participanteEnrolador: Participante;
  public nombres: string;
  public apellidoPaterno: string;
  public apellidoMaterno: string;
  public correo: string;
  public numeroTelefono: string;
  public fechaAlta: Date;
  public nombreCompleto: string;
}
