import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Usuario } from '../models/usuario';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }
  usuario$: Subject<Usuario> = new Subject<Usuario>();

  constructor(
    private http: HttpClient,
    private router: Router) {

    if(localStorage.getItem('user')){
      this.usuario$.next(JSON.parse(localStorage.getItem('user')));
    } else {
      this.usuario$.next(new Usuario());
    }
  }

  isUserInSession(): boolean {
    if (localStorage.getItem('user')) {
      return true;
    } else {
      return false;
    }
  }

  login(usuario: Usuario ): Observable<any> {
    return this.http.post(environment.api + 'usuario/authenticate', usuario,  this.config);
  }

  getUser(): Usuario {
    let usuario: Usuario;
    if (localStorage.getItem('user')) {
      usuario= JSON.parse(localStorage.getItem('user'));
    } else {
      usuario = null;
    }
    this.usuario$.next(usuario);
    return usuario;
  }

  getUser$(): Observable<Usuario> {
    return this.usuario$.asObservable();
  }

  setUser(usuario: Usuario) {
    localStorage.setItem('user', JSON.stringify(usuario));
    this.usuario$.next(usuario);
  }

  logout() {
    localStorage.removeItem('user');
    this.router.navigateByUrl("/login");
  }

}
