import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Evento } from '../models/evento';
import { ParticipanteEvento } from '../models/participante-evento';
import { TipoEvento } from '../models/tipo-evento';
import { Movimiento } from '../models/movimiento';
import { Participante } from '../models/participante';

@Injectable({
  providedIn: 'root'
})
export class MovimientosService {

  config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }

  apiRoute = environment.api + 'movimientos';

  constructor(
    private http: HttpClient) {
  }

  findAll(): Observable<any> {
    return this.http.get(this.apiRoute);
  }

  crear(movimiento: Movimiento): Observable<any> {
    return this.http.post(this.apiRoute, movimiento );
  }

  crearParticipanteMovimiento(movimiento: Movimiento): Observable<any> {
    movimiento.fecha = new Date();
    return this.http.post(this.apiRoute, movimiento );
  }

  actualizar(movimiento: Movimiento): Observable<any> {
    return this.http.put(this.apiRoute + '/' + movimiento.id , movimiento );
  }

  eliminar(movimiento: Movimiento): Observable<any> {
    return this.http.delete(this.apiRoute + '/' + movimiento.id);
  }

  findByParticipante(participante: Participante): Observable<any> {
    return this.http.get(this.apiRoute + '/participante/' + participante.id);
  }

  findByEvento(evento: Evento): Observable<any> {
    return this.http.get(this.apiRoute + '/evento/' + evento.id);
  }

  findByEventoAndParticipante(evento: Evento, participante: Participante): Observable<any> {
    return this.http.get(this.apiRoute +'/evento/' + evento.id + '/participante/' + participante.id);
  }

}
