import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Participante } from '../models/participante';

@Injectable({
  providedIn: 'root'
})
export class ParticipanteService {

  config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }

  apiRoute = environment.api + 'participantes';

  constructor(
    private http: HttpClient) {
  }

  findAll(): Observable<any> {
    return this.http.get(this.apiRoute);
  }

  crear(participante: Participante): Observable<any> {
    return this.http.post(this.apiRoute,participante);
  }

  consultaById(id : number): Observable<any> {
    return this.http.get(this.apiRoute + '/'+id);
  }

  deleteById(participante: Participante): Observable<any> {
    return this.http.delete(this.apiRoute + '/'+participante.id);
  }

  actualizar(participante: Participante): Observable<any> {
    return this.http.put(this.apiRoute + '/'+participante.id, participante);
  }

  searchByName(nombres: string): Observable<any> {
    return this.http.get(this.apiRoute + '/search/' + nombres);
  }

  findByParticipanteEnrolador(participante: Participante): Observable<any> {
    return this.http.get(this.apiRoute + '/participanteEnrolador/' + participante.id);
  }

}
