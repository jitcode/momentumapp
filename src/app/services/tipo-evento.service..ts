import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Evento } from '../models/evento';
import { ParticipanteEvento } from '../models/participante-evento';
import { TipoEvento } from '../models/tipo-evento';

@Injectable({
  providedIn: 'root'
})
export class TipoEventoService {

  config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }

  apiRoute = environment.api + 'tipoevento';

  constructor(
    private http: HttpClient) {
  }

  findAll(): Observable<any> {
    return this.http.get(this.apiRoute);
  }

  crear(tipoEvento: TipoEvento): Observable<any> {
    return this.http.post(this.apiRoute, tipoEvento );
  }

  actualizar(tipoEvento: TipoEvento): Observable<any> {
    return this.http.put(this.apiRoute + '/' + tipoEvento.id , tipoEvento );
  }

  eliminar(tipoEvento: TipoEvento): Observable<any> {
    return this.http.delete(this.apiRoute + '/' + tipoEvento.id);
  }

}
