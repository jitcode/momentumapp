import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CatalogosService {

  config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }

  apiRoute = environment.api + 'catalogos';

  constructor(
    private http: HttpClient) {
  }

  findTiposMovimientos(): Observable<any> {
    return this.http.get(this.apiRoute + '/tipomovimiento');
  }

}
