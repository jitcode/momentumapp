import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MetodoPago } from '../models/metodo-pago';

@Injectable({
  providedIn: 'root'
})
export class MetodoPagoService {

  config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }

  apiRoute = environment.api + 'metodopago';

  constructor(
    private http: HttpClient) {
  }

  findAll(): Observable<any> {
    return this.http.get(this.apiRoute);
  }

  crear(metodoPago: MetodoPago): Observable<any> {
    return this.http.post(this.apiRoute, metodoPago );
  }

  actualizar(metodoPago: MetodoPago): Observable<any> {
    return this.http.put(this.apiRoute + '/' + metodoPago.id , metodoPago );
  }

  eliminar(metodoPago: MetodoPago): Observable<any> {
    return this.http.delete(this.apiRoute + '/' + metodoPago.id);
  }

}
