import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Evento } from '../models/evento';
import { ParticipanteEvento } from '../models/participante-evento';

@Injectable({
  providedIn: 'root'
})
export class EventoService {

  config = {
    headers: {
      'Content-Type': 'application/json'
    }
  }

  constructor(
    private http: HttpClient) {
  }

  findAll(): Observable<any> {
    return this.http.get(environment.api + 'evento');
  }

  findProximos(): Observable<any> {
    return this.http.get(environment.api + 'evento/proximos');
  }

  crear(evento: Evento): Observable<any> {
    return this.http.post(environment.api + 'evento', evento);
  }

  actualizar(evento: Evento): Observable<any> {
    return this.http.put(environment.api + 'evento/' + evento.id, evento);
  }

  consultaById(id : number): Observable<any> {
    return this.http.get(environment.api + 'evento/'+ id);
  }

  deleteById(evento: Evento): Observable<any> {
    return this.http.delete(environment.api + 'evento/' + evento.id);
  }

  consultaParticipantes(evento: Evento): Observable<any> {
    return this.http.get(environment.api + 'evento/' + evento.id + '/participante');
  }

  agregarParticipante(participanteEvento: ParticipanteEvento): Observable<any> {
    return this.http.post(environment.api + 'evento/' + participanteEvento.evento.id + '/participante', participanteEvento);
  }

  actualizarParticipante(participanteEvento: ParticipanteEvento): Observable<any> {
    return this.http.put(environment.api + 'evento/' + participanteEvento.evento.id + '/participante/' + participanteEvento.participante.id, participanteEvento);
  }

  eliminarParticipante(participanteEvento: ParticipanteEvento): Observable<any> {
    return this.http.delete(environment.api + 'evento/' + participanteEvento.evento.id + '/participante/' + participanteEvento.participante.id);
  }

  asistioParticipante(participanteEvento: ParticipanteEvento): Observable<any> {
    return this.http.patch(environment.api + 'evento/' + participanteEvento.evento.id + '/participante/' + participanteEvento.participante.id + '/asistio', null);
  }

  graduadoParticipante(participanteEvento: ParticipanteEvento): Observable<any> {
    return this.http.patch(environment.api + 'evento/' + participanteEvento.evento.id + '/participante/' + participanteEvento.participante.id + '/graduado', null);
  }

  searchByDescripcion(filterValue: string) : Observable<any> {
    return this.http.get(environment.api + 'evento/search/'+filterValue);
  }

}
