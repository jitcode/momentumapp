import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from '../services/login.service';
import { Usuario } from '../models/usuario';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private loginService: LoginService,
    private toastr: ToastrService,
    private spinner: NgxSpinnerService) {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Obtenemos el token
    const user: Usuario = this.loginService.getUser();

    // Importante: modificamos de forma inmutable, haciendo el clonado de la petición
    if (user) {
      request = request.clone({
        headers: request.headers.set('Authorization', user.token)
      });
    }

    request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

    this.spinner.show();

    // Pasamos al siguiente interceptor de la cadena la petición modificada
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        this.spinner.hide();
        if (event instanceof HttpResponse) {
          if (event.status === 200 && event.body != null && event.body.error != undefined && event.body.error != null) {
            this.toastr.error(event.body.message);
          }
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        this.spinner.hide();
        if (error.status === 403) {
          this.loginService.logout();
          this.toastr.info("Se ha cerrado su sesión por inactividad. Favor de iniciar sesión nuevamente.");
        } else if (error.status === 400) {
          this.toastr.error(error.error.message);
        } else if (error.status === 404) {
          this.toastr.info("No se encontro información.");
        } else {
          this.toastr.error("Ocurrio un error. Favor de contactar a sistemas");
        }
        return throwError(error);
      })
    );
  }
}
