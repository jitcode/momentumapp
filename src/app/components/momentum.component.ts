import { Component, OnInit } from '@angular/core';
import { Usuario } from '../models/usuario';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-momentum',
  templateUrl: './momentum.component.html',
  styleUrls: ['./momentum.component.scss']
})
export class MomentumComponent implements OnInit {

  public user: Usuario;

  constructor(
    private loginService: LoginService,
    private router: Router
    ) {
    this.user = this.loginService.getUser();
  }

  ngOnInit() {}

  logout() {
    this.loginService.logout();
  }

  isActiveRoute(route: string): boolean {
    return this.router.url == route;
  }

}
