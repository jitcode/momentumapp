import { Component, OnInit } from '@angular/core';
import { Menu } from 'src/app/models/menu';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  listaMenus: Array<Menu> = new Array<Menu>();



  constructor() {
    this.listaMenus = [
      { titulo : 'Dashboard' , route : '/momentum/home', icon_class : '' },
      { titulo : 'Eventos' , route : '/momentum/eventos', icon_class : '' },
      { titulo : 'Participantes' , route : '/momentum/eventos', icon_class : '' }
    ];

   }

  ngOnInit() {
  }

}
