import { Component, OnInit } from '@angular/core';
import { Participante } from 'src/app/models/participante';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { ParticipanteService } from 'src/app/services/participante.service';
import { ParticipantesEnroladosDialog } from './participantes-enrolados-dialog/participantes-enrolados.dialog';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-participantes',
  templateUrl: './participantes.component.html',
  styleUrls: ['./participantes.component.scss']
})
export class ParticipantesComponent implements OnInit {

  listaParticipantes: Array<Participante> = new Array<Participante>();

  constructor(
    private router: Router,
    private toastr: ToastrService,
    public dialog: MatDialog,
    private participanteService: ParticipanteService
  ) { }

  ngOnInit() {

    this.consultarTodos();
  }

  consultarTodos() {
    this.participanteService.findAll().subscribe(response => {
      this.listaParticipantes = response;
    })
  }

  consultarByNombre(nombre: string){
    this.participanteService.searchByName(nombre).subscribe(response => {
      this.listaParticipantes = response;
    })
  }

  verEnrolados(participante: Participante) {
    let dialogRef = this.dialog.open(ParticipantesEnroladosDialog, {
      data: participante,
      maxHeight: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  ver(participante: Participante) {
    this.router.navigateByUrl('/momentum/participantes/view/'+participante.id);
  }

  eliminar(participante: Participante) {
    this.participanteService.deleteById(participante).subscribe(response => {
      this.toastr.info("Se ha eliminado correctamente el participante : " + participante.nombres);
      this.consultarTodos();
    });
  }

  onSearchChange(searchValue: string): void {
    if(searchValue.length>=3){
      this.consultarByNombre(searchValue);
    } else if(searchValue.length==0 ){
      this.consultarTodos();
    }
  }

}
