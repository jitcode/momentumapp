import { Component, Inject, ViewChild, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Participante } from 'src/app/models/participante';
import { Evento } from 'src/app/models/evento';
import { MetodoPago } from 'src/app/models/metodo-pago';
import { MetodoPagoService } from 'src/app/services/metodo-pago.service';
import { Movimiento } from 'src/app/models/movimiento';
import { MovimientosService } from 'src/app/services/movimientos.service';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ParticipanteService } from 'src/app/services/participante.service';

@Component({
selector: 'participantes-enrolados-dialog',
templateUrl: 'participantes-enrolados.dialog.html',
styleUrls: ['./participantes-enrolados.dialog.scss']
})
export class ParticipantesEnroladosDialog implements OnInit {

  isLoadingResult: boolean;

  nombreParticipanteEnrolador: string = '';
  listaParticipantes: Array<Participante> = new Array<Participante>();

  //TABLA MOVIMIENTOS
  displayedColumns: string[] = ['nombre', 'fecha'];
  dataSource: MatTableDataSource<Participante>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    public dialogRef: MatDialogRef<ParticipantesEnroladosDialog>,
    @Inject(MAT_DIALOG_DATA) public participante: Participante,
    private participanteService: ParticipanteService,
    private toastr: ToastrService) {
      this.consultaListaParticipantes();
  }

  ngOnInit(){
  }

  consultaListaParticipantes() {
    this.participanteService.findByParticipanteEnrolador(this.participante).subscribe(response => {
      this.listaParticipantes = response;
      this.initDataSource();
    })
  }

  initDataSource() {
    this.dataSource = new MatTableDataSource<Participante>(this.listaParticipantes);

    this.dataSource.sort = this.sort;
    this.paginator._intl.itemsPerPageLabel = "";
    this.dataSource.paginator = this.paginator;
  }

  cerrar(){
    this.dialogRef.close();
  }

}
