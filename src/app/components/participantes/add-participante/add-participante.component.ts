import { Component, OnInit } from '@angular/core';
import { Participante } from 'src/app/models/participante';
import { ToastrService } from 'ngx-toastr';
import { ParticipanteService } from 'src/app/services/participante.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-participante',
  templateUrl: './add-participante.component.html',
  styleUrls: ['./add-participante.component.scss']
})
export class AddParticipanteComponent implements OnInit {

  participante: Participante = new Participante();

  keyword = "nombreCompleto";
  isLoadingResult: boolean;
  filteredParticipantesEnrolador: Array<Participante> = new Array<Participante>();
  participanteEnrolador: Participante = new Participante();
  nombreParticipanteEnrolador: string = '';

  constructor(
    private toastr: ToastrService,
    private participanteService: ParticipanteService,
    private router: Router
    ) { }

  ngOnInit() {
  }

  agregar() {

    if(this.participanteEnrolador
      && this.participanteEnrolador.id != undefined
      && this.participanteEnrolador.id != null) {

      this.participante.participanteEnrolador = this.participanteEnrolador

    }

    this.participanteService.crear(this.participante).subscribe(response => {
      this.router.navigateByUrl('/momentum/participantes');
    });
  }

  filterParticipantesEnrolador(value: String) {
    let filterValue = value.toUpperCase();

    if(filterValue!='') {
    this.isLoadingResult = true;

      this.participanteService.searchByName(filterValue).subscribe(response => {
        if(response == undefined) {
          this.filteredParticipantesEnrolador = new Array<Participante>();
        } else {
          this.filteredParticipantesEnrolador = response;
        }

        this.isLoadingResult = false;
      });
    } else {
      this.filteredParticipantesEnrolador = new Array<Participante>();
    }
  }

  selectParticipanteEnrolador(participante: Participante): string{
    if(participante!=null) {
      this.participanteEnrolador = participante;
      return participante.nombres + ' ' + participante.apellidoPaterno + ' ' + participante.apellidoMaterno;
    } else {
      return '';
    }
  }

  selectEventEnrolador(item) {
    this.selectParticipanteEnrolador(item);
  }

  onChangeSearchEnrolador(search: string) {
    this.filterParticipantesEnrolador(search);
  }


}
