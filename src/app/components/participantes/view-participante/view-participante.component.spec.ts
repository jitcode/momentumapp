import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewParticipanteComponent } from './view-participante.component';

describe('ViewParticipanteComponent', () => {
  let component: ViewParticipanteComponent;
  let fixture: ComponentFixture<ViewParticipanteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewParticipanteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewParticipanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
