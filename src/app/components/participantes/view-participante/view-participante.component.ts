import { Component, OnInit } from '@angular/core';
import { Participante } from 'src/app/models/participante';
import { ParticipanteService } from 'src/app/services/participante.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-view-participante',
  templateUrl: './view-participante.component.html',
  styleUrls: ['./view-participante.component.scss']
})
export class ViewParticipanteComponent implements OnInit {
  participante: Participante = new Participante();

  keyword = "nombreCompleto";
  isLoadingResult: boolean = false;
  filteredParticipantesEnrolador: Array<Participante> = new Array<Participante>();
  participanteEnrolador: Participante = new Participante();
  nombreParticipanteEnrolador: string = '';

  constructor(
    private participanteService: ParticipanteService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private router: Router
    ) {
      this.participante.id = this.activatedRoute.snapshot.params.id;
      this.consultar();
     }

  ngOnInit() {
  }

  consultar() {
    this.participanteService.consultaById(this.participante.id).subscribe(response => {
      this.participante = response;

      if(this.participante.participanteEnrolador){
        this.participanteEnrolador = this.participante.participanteEnrolador;
        this.nombreParticipanteEnrolador = this.participanteEnrolador.nombreCompleto;
        this.filteredParticipantesEnrolador.push(this.participanteEnrolador);
      }
    });
  }

  guardar() {

    if(this.participanteEnrolador
      && this.participanteEnrolador.id != undefined
      && this.participanteEnrolador.id != null) {

      this.participante.participanteEnrolador = this.participanteEnrolador

    }

    this.participanteService.actualizar(this.participante).subscribe(response => {
      this.toastr.info('Se ha guardado correctamente la información del participante.');
      this.router.navigateByUrl('/momentum/participantes');
    });
  }

  filterParticipantesEnrolador(value: String) {
    let filterValue = value.toUpperCase();

    if(filterValue!='') {
    this.isLoadingResult = true;

      this.participanteService.searchByName(filterValue).subscribe(response => {
        if(response == undefined) {
          this.filteredParticipantesEnrolador = new Array<Participante>();
        } else {
          this.filteredParticipantesEnrolador = response;
        }

        this.isLoadingResult = false;
      });
    } else {
      this.filteredParticipantesEnrolador = new Array<Participante>();
    }
  }

  selectParticipanteEnrolador(participante: Participante): string{
    if(participante!=null) {
      this.participanteEnrolador = participante;
      return participante.nombres + ' ' + participante.apellidoPaterno + ' ' + participante.apellidoMaterno;
    } else {
      return '';
    }
  }

  selectEventEnrolador(item) {
    this.selectParticipanteEnrolador(item);
  }

  onChangeSearchEnrolador(search: string) {
    this.filterParticipantesEnrolador(search);
  }

}
