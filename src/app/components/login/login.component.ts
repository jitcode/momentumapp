import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { Usuario } from 'src/app/models/usuario';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  correo = '';
  password = '';


  constructor(private loginService: LoginService,
    private router: Router) { }

  ngOnInit() {
  }

  login() {
    console.log(this.correo, this.password);
    let usuario: Usuario = new Usuario();

    usuario.correo = this.correo;
    usuario.password = this.password;

    this.loginService.login(usuario).subscribe(response => {
      if(!response.error){
        usuario = response;

        this.loginService.setUser(usuario);
        this.router.navigateByUrl('/momentum/home');
      }
    })
  }

}
