import { Component, OnInit } from '@angular/core';
import { Evento } from 'src/app/models/evento';
import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { EventoService } from 'src/app/services/eventos.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { TipoEvento } from 'src/app/models/tipo-evento';
import { TipoEventoService } from 'src/app/services/tipo-evento.service.';

@Component({
  selector: 'app-add-evento',
  templateUrl: './add-evento.component.html',
  styleUrls: ['./add-evento.component.scss']
})
export class AddEventoComponent implements OnInit {
  keyword = "descripcion";
  isLoadingResult: boolean;

  filteredTipoEventos: Array<TipoEvento> = new Array<TipoEvento>();

  evento: Evento = new Evento();

  hoveredDate: NgbDate;

  fromDate: NgbDate;
  toDate: NgbDate;

  fromText: string;
  toText: string;

  constructor(
    calendar: NgbCalendar,
    private eventoService: EventoService,
    private toastr: ToastrService,
    private router: Router,
    private tipoEventoService: TipoEventoService) {

    this.fromDate = calendar.getToday();
    this.toDate = calendar.getNext(calendar.getToday(), 'd', 2);
    this.consultarTipoEventos();
  }

  ngOnInit(): void {
  }

  consultarTipoEventos(){
    this.tipoEventoService.findAll().subscribe(response => {
      this.filteredTipoEventos = response;
    })
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }

    if(this.fromDate!=null)
      this.fromText = this.fromDate.year + '/' + this.fromDate.month + '/' + this.fromDate.day;
    if(this.toDate!=null)
      this.toText = this.toDate.year + '/' + this.toDate.month + '/' + this.toDate.day;
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  agregar() {

    this.evento.fechaInicio = new Date(this.fromDate.year, this.fromDate.month-1, this.fromDate.day) ;
    this.evento.fechaFin = new Date(this.toDate.year , this.toDate.month-1, this.toDate.day ) ;

    this.eventoService.crear(this.evento).subscribe( response => {
      if(!response.error) {
        this.toastr.success("Evento agregado con exito!");
        this.router.navigateByUrl("/momentum/eventos");
      }
    });
  }

  selectEvent(item: TipoEvento) {
    this.evento.tipoEvento = item;
  }

}
