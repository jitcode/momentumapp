import { Component, OnInit } from '@angular/core';
import { Evento } from 'src/app/models/evento';
import { EventoService } from 'src/app/services/eventos.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'card-proximos-eventos',
  templateUrl: './proximos-eventos.component.html',
  styleUrls: ['./proximos-eventos.component.scss']
})
export class ProximosEventosComponent implements OnInit {

  listaEventos: Array<Evento> = new Array<Evento>();

  constructor(
    private eventoService: EventoService,
    private toastr: ToastrService,
    private router: Router) {
    this.consultaEventos();
  }

  consultaEventos() {
    this.eventoService.findProximos().subscribe(response => {
      if(!response.error) {
        this.listaEventos = response;
      }
    })
  }

  ngOnInit() {
  }

  ver(evento: Evento) {
    this.router.navigateByUrl('/momentum/eventos/view/'+evento.id);
  }

  eliminar(evento: Evento) {
    this.eventoService.deleteById(evento).subscribe(response => {
      this.toastr.info("Se ha eliminado correctamente el evento : " + evento.tipoEvento.descripcion);
      this.consultaEventos();
    });
  }

  editar(evento: Evento) {
    this.router.navigateByUrl('/momentum/eventos/edit/'+evento.id);
  }

}
