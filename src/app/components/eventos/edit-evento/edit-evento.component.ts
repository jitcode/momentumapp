import { Component, OnInit } from '@angular/core';
import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { TipoEvento } from 'src/app/models/tipo-evento';
import { Evento } from 'src/app/models/evento';
import { EventoService } from 'src/app/services/eventos.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { TipoEventoService } from 'src/app/services/tipo-evento.service.';
import { CalendarEventosComponent } from '../calendar-eventos/calendar-eventos.component';

@Component({
  selector: 'app-edit-evento',
  templateUrl: './edit-evento.component.html',
  styleUrls: ['./edit-evento.component.scss']
})
export class EditEventoComponent implements OnInit {
  keyword = "descripcion";
  isLoadingResult: boolean;

  filteredTipoEventos: Array<TipoEvento> = new Array<TipoEvento>();

  evento: Evento = new Evento();

  hoveredDate: NgbDate;

  fromDate: NgbDate;
  toDate: NgbDate;

  fromText: string;
  toText: string;

  seleccionTipoEvento: number = 0;

  constructor(
    calendar: NgbCalendar,
    private eventoService: EventoService,
    private toastr: ToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private tipoEventoService: TipoEventoService) {
    this.consultarTipoEventos();


  }

  ngOnInit(): void {
  }

  consultarEvento(){
    this.evento.id = this.activatedRoute.snapshot.params.id;
    this.eventoService.consultaById(this.evento.id).subscribe(response => {
      this.evento = response;
      this.seleccionTipoEvento = this.evento.tipoEvento.id;
      let fechaInicio: Date = new Date(this.evento.fechaInicio);
      let fechaFin: Date = new Date(this.evento.fechaFin);
      this.fromDate = new NgbDate(fechaInicio.getFullYear(), fechaInicio.getMonth()+1, fechaInicio.getDate());
      this.toDate = new NgbDate(fechaFin.getFullYear(), fechaFin.getMonth()+1, fechaFin.getDate());

      if(this.fromDate!=null)
      this.fromText = this.fromDate.year + '/' + this.fromDate.month + '/' + this.fromDate.day;
      if(this.toDate!=null)
        this.toText = this.toDate.year + '/' + this.toDate.month + '/' + this.toDate.day;

    });
  }

  consultarTipoEventos(){
    this.tipoEventoService.findAll().subscribe(response => {
      this.filteredTipoEventos = response;
      this.consultarEvento();
    })
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }

    if(this.fromDate!=null)
      this.fromText = this.fromDate.year + '/' + this.fromDate.month + '/' + this.fromDate.day;
    if(this.toDate!=null)
      this.toText = this.toDate.year + '/' + this.toDate.month + '/' + this.toDate.day;
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  agregar() {

    this.evento.fechaInicio = new Date(this.fromDate.year, this.fromDate.month-1, this.fromDate.day) ;
    this.evento.fechaFin = new Date(this.toDate.year , this.toDate.month-1, this.toDate.day ) ;

    this.eventoService.actualizar(this.evento).subscribe( response => {
      if(!response.error) {
        this.toastr.success("Evento agregado con exito!");
        this.router.navigateByUrl("/momentum/eventos");
      }
    });
  }

  selectEvent(item: TipoEvento) {
    this.evento.tipoEvento = item;
  }

  compareIds(o1: TipoEvento, o2: TipoEvento): boolean {
    return o1 && o2 && o1.id === o2.id;
  }

}
