import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Movimiento } from 'src/app/models/movimiento';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MovimientosService } from 'src/app/services/movimientos.service';
import { Evento } from 'src/app/models/evento';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ParticipanteEvento } from 'src/app/models/participante-evento';
import { MovimientoEventoDialog } from '../movimiento-evento-dialog/movimiento-evento.dialog';
import { EventoService } from 'src/app/services/eventos.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'card-movimientos-evento',
  templateUrl: './card-movimientos-evento.component.html',
  styleUrls: ['./card-movimientos-evento.component.scss']
})
export class CardMovimientosEventoComponent implements OnInit {

  listaMovimientos: Array<Movimiento> = new Array<Movimiento>();
  listaMovimientosFiltered: Array<Movimiento> = new Array<Movimiento>();

  evento: Evento = new Evento();

  displayedColumns: string[] = ['descripcion', 'fecha', 'cantidad', 'metodoPago','acciones'];
  dataSource: MatTableDataSource<Movimiento>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private activatedRoute: ActivatedRoute,
    private movimientosService: MovimientosService,
    private eventoService: EventoService,
    public dialog: MatDialog,
    private toastr: ToastrService
  ) {
    this.evento.id = this.activatedRoute.snapshot.params.id;
    this.consultaMovimientos();
    this.ConsultarEvento();
  }

  ngOnInit() {
  }

  ConsultarEvento() {
    this.eventoService.consultaById(this.evento.id).subscribe(response => {
      if(!response.error) {
        this.evento = response;
      }
    });
  }

  consultaMovimientos(){
    this.movimientosService.findByEvento(this.evento).subscribe(response => {
      this.listaMovimientos = response;
      this.listaMovimientosFiltered = this.listaMovimientos;
      this.crearDataSource();
    });
  }

  crearDataSource(){
    this.dataSource = new MatTableDataSource<Movimiento>(this.listaMovimientosFiltered);
    this.paginator._intl.itemsPerPageLabel = '';
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openDialog(){
    let participanteEvento: ParticipanteEvento  = new ParticipanteEvento();
    participanteEvento.evento = this.evento;
    let dialogRef = this.dialog.open(MovimientoEventoDialog, {
      data: {eventoParticipante : participanteEvento, movimiento: new Movimiento() },
      maxHeight: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
        this.consultaMovimientos();
    });
  }

  openDialogMovimiento(movimiento: Movimiento){
    let participanteEvento: ParticipanteEvento  = new ParticipanteEvento();
    participanteEvento.evento = this.evento;
    let dialogRef = this.dialog.open(MovimientoEventoDialog, {
      data: {eventoParticipante : participanteEvento, movimiento: movimiento },
      maxHeight: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
        this.consultaMovimientos();
    });
  }

  verPago(movimiento: Movimiento){
    this.openDialogMovimiento(movimiento);
  }

  eliminarPago(movimiento: Movimiento){
    this.movimientosService.eliminar(movimiento).subscribe(response => {
      this.toastr.info("Se ha eliminado el movimiento.");
      this.consultaMovimientos();
    })
  }

  getTotalPagado(): number{
    let sum = 0;

    sum = this.listaMovimientos.reduce((prev, cur) => {
      return prev + (cur.tipoMovimiento.id == 1? cur.cantidad: cur.cantidad * -1);
    }, 0);

    return sum;
  }

}
