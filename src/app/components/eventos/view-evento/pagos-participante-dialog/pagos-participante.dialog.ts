import { Component, Inject, ViewChild, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Participante } from 'src/app/models/participante';
import { Evento } from 'src/app/models/evento';
import { MetodoPago } from 'src/app/models/metodo-pago';
import { MetodoPagoService } from 'src/app/services/metodo-pago.service';
import { Movimiento } from 'src/app/models/movimiento';
import { MovimientosService } from 'src/app/services/movimientos.service';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ParticipanteEvento } from 'src/app/models/participante-evento';

@Component({
selector: 'dialog-add-participante',
templateUrl: 'pagos-participante.dialog.html',
styleUrls: ['./pagos-participante.dialog.scss']
})
export class PagosParticipanteDialog implements OnInit {
  noMovimiento: number = 0;
  monto: string = '';
  metodoPago: MetodoPago = new MetodoPago();
  listaMetodosPago: Array<MetodoPago> = new Array<MetodoPago>();
  listaMovimientos: Array<Movimiento> = new Array<Movimiento>();

  isLoadingResult: boolean;

  nombreParticipanteEnrolador: string = '';

  movimientoSeleccionado: Movimiento = new Movimiento();

  //TABLA MOVIMIENTOS
  displayedColumns: string[] = ['noMovimiento', 'descripcion', 'fecha', 'cantidad', 'metodoPago','acciones'];
  dataSource: MatTableDataSource<Movimiento>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    public dialogRef: MatDialogRef<PagosParticipanteDialog>,
    @Inject(MAT_DIALOG_DATA) public eventoParticipante: ParticipanteEvento,
    private metodoPagoService: MetodoPagoService,
    private movimientosService: MovimientosService,
    private toastr: ToastrService) {

    this.consultarMetodoPago();
    this.consultarMovimientos();
  }

  ngOnInit(){

   this.initDataSource();

  }

  getTotalPagado(): number{
    let sum = 0;

    sum = this.listaMovimientos.reduce((prev, cur) => {
      return prev + cur.cantidad;
    }, 0);

    return sum;
  }

  consultarMovimientos() {
    this.movimientosService
      .findByEventoAndParticipante(this.eventoParticipante.evento, this.eventoParticipante.participante)
      .subscribe(response => {
        this.listaMovimientos = response;
        this.initDataSource();
    })
  }

  initDataSource() {
    this.dataSource = new MatTableDataSource<Movimiento>(this.listaMovimientos);

    this.dataSource.sort = this.sort;
    //this.paginator._intl.itemsPerPageLabel = "";
    //this.dataSource.paginator = this.paginator;
  }

  consultarMetodoPago() {
    this.metodoPagoService
      .findAll()
      .subscribe(response => {
      this.listaMetodosPago = response;
    })
  }

  guardar() {
    let movimiento: Movimiento = new Movimiento();

    if(this.movimientoSeleccionado && this.movimientoSeleccionado.id) {
      movimiento = this.movimientoSeleccionado;
      movimiento.metodoPago = this.metodoPago;
      movimiento.cantidad = Number(this.monto.replace("$",""));
    } else {
      movimiento.cantidad = Number(this.monto.replace("$",""));
      movimiento.participante = this.eventoParticipante.participante;
      movimiento.evento = this.eventoParticipante.evento;
      movimiento.metodoPago = this.metodoPago;
      movimiento.descripcion = 'Pago de '
        + this.eventoParticipante.evento.tipoEvento.descripcion + ' '
        + this.eventoParticipante.evento.generacion + ' '
        + this.eventoParticipante.participante.nombreCompleto;
      movimiento.tipoMovimiento = {
        id: 1,
        descripcion: 'Abono'
      };
    }

     this.movimientosService
      .crearParticipanteMovimiento(movimiento)
      .subscribe(response => {
        this.toastr.info("Se ha agregado el pago correctamente.");
        this.limpiarCampos();
        this.consultarMovimientos();
     })

  }

  limpiarCampos(){
    this.metodoPago = new MetodoPago();
    this.monto = '';
    this.noMovimiento = 0;
    this.movimientoSeleccionado = new Movimiento();
  }

  compareIds(o1: MetodoPago, o2: MetodoPago): boolean {
    return o1 && o2 && o1.id === o2.id;
  }

  verPago(movimiento: Movimiento){
    this.monto = '$ ' + movimiento.cantidad;
    this.metodoPago = movimiento.metodoPago;
    this.noMovimiento = movimiento.id;
    this.movimientoSeleccionado = movimiento;
  }

  eliminarPago(movimiento: Movimiento){
    this.movimientosService.eliminar(movimiento).subscribe(response => {
      this.toastr.info("Se ha eliminado el movimiento.");
      this.consultarMovimientos();
      this.limpiarCampos();
    })
  }

  mostrarBotonEliminar(): boolean {
    let mostrar = false;

    if(this.movimientoSeleccionado && this.movimientoSeleccionado.id){
      mostrar = true;
    }

    return mostrar;
  }

  padWithZeros(num:number, size:number): string {
    let s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
  }

}
