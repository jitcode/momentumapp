import { Component, Inject, ViewChild, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Participante } from 'src/app/models/participante';
import { Evento } from 'src/app/models/evento';
import { MetodoPago } from 'src/app/models/metodo-pago';
import { MetodoPagoService } from 'src/app/services/metodo-pago.service';
import { Movimiento } from 'src/app/models/movimiento';
import { MovimientosService } from 'src/app/services/movimientos.service';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { ParticipanteEvento } from 'src/app/models/participante-evento';
import { TipoMovimiento } from 'src/app/models/tipo-movimiento';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { ParticipanteService } from 'src/app/services/participante.service';

@Component({
selector: 'movimiento-evento-dialog',
templateUrl: 'movimiento-evento.dialog.html',
styleUrls: ['./movimiento-evento.dialog.scss']
})
export class MovimientoEventoDialog implements OnInit {

  descripcionEvento: string = '';
  descripcion:string = '';
  monto: string = '';
  metodoPago: MetodoPago = new MetodoPago();
  listaMetodosPago: Array<MetodoPago> = new Array<MetodoPago>();
  listaMovimientos: Array<Movimiento> = new Array<Movimiento>();

  isLoadingResult: boolean;

  tipoMovimiento: TipoMovimiento = {id: 1, descripcion: 'Abono'};
  listaTiposMovimientos: Array<TipoMovimiento> = new Array<TipoMovimiento>();

  keyword: string = 'nombreCompleto';
  filteredParticipantes: Array<Participante> = new Array<Participante>();
  participante: Participante = new Participante();
  nombreParticipante: string = '';

  constructor(
    public dialogRef: MatDialogRef<MovimientoEventoDialog>,
    @Inject(MAT_DIALOG_DATA) public data : { eventoParticipante: ParticipanteEvento , movimiento: Movimiento},
    private metodoPagoService: MetodoPagoService,
    private movimientosService: MovimientosService,
    private catalogosService: CatalogosService,
    private participanteService: ParticipanteService,
    private toastr: ToastrService) {

    if(this.data.movimiento && this.data.movimiento.id){
      this.descripcion = this.data.movimiento.descripcion;
      this.monto = '$ ' + this.data.movimiento.cantidad;
      this.metodoPago = this.data.movimiento.metodoPago;
      this.tipoMovimiento = this.data.movimiento.tipoMovimiento;
      this.participante = this.data.movimiento.participante;
    }

    if(this.data.eventoParticipante.participante) {
      this.participante = this.data.eventoParticipante.participante;
    }

    if(this.participante){
      this.nombreParticipante = this.participante.nombreCompleto;
    }

    this.descripcionEvento = this.data.eventoParticipante.evento.tipoEvento.descripcion + ' Invictus ' +  this.data.eventoParticipante.evento.generacion;

    this.consultarMetodoPago();
    this.consultarTiposMovimientos();
  }

  ngOnInit(){
  }

  getTotalPagado(): number{
    let sum = 0;

    sum = this.listaMovimientos.reduce((prev, cur) => {
      return prev + cur.cantidad;
    }, 0);

    return sum;
  }

  consultarTiposMovimientos() {
    this.catalogosService.findTiposMovimientos().subscribe(response => {
      this.listaTiposMovimientos = response;
    })
  }


  consultarMetodoPago() {
    this.metodoPagoService
      .findAll()
      .subscribe(response => {
      this.listaMetodosPago = response;
    })
  }

  guardar() {
    let movimiento: Movimiento = new Movimiento();

    if(this.data.movimiento && this.data.movimiento.id){
      movimiento.id = this.data.movimiento.id;
    }

    movimiento.cantidad = Number(this.monto.replace("$",""));
    if(this.participante && this.participante.id)
      movimiento.participante = this.participante;
    else
      delete movimiento.participante;
    movimiento.evento = this.data.eventoParticipante.evento;
    movimiento.metodoPago = this.metodoPago;
    movimiento.tipoMovimiento = this.tipoMovimiento;
    movimiento.descripcion = this.descripcion;

    this.movimientosService
      .crearParticipanteMovimiento(movimiento)
      .subscribe(response => {
        this.toastr.info("Se ha guardado el pago correctamente.");
        this.limpiarCampos();
     })

    this.dialogRef.close();

  }

  limpiarCampos(){
    this.metodoPago = new MetodoPago();
    this.monto = '';
  }

  compareIds(o1: any, o2: any): boolean {
    return o1 && o2 && o1.id === o2.id;
  }

  filterParticipantes(value: String) {
    let filterValue = value.toUpperCase();

    if(filterValue!='') {
    this.isLoadingResult = true;

      this.participanteService.searchByName(filterValue).subscribe(response => {
        if(response == undefined) {
          this.filteredParticipantes = new Array<Participante>();
        } else {
          this.filteredParticipantes = response;
        }

        this.isLoadingResult = false;
      });
    } else {
      this.filteredParticipantes = new Array<Participante>();
    }
  }

  selectParticipante(participante: Participante): string{
    if(participante!=null) {
      this.participante = participante;
      return participante.nombres + ' ' + participante.apellidoPaterno + ' ' + participante.apellidoMaterno;
    } else {
      return '';
    }
  }

  selectEvent(item) {
    this.selectParticipante(item);
  }

  onChangeSearch(search: string) {
    this.filterParticipantes(search);
  }

}
