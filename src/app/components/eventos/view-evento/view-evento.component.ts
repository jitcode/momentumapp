import { Component, OnInit, Inject } from '@angular/core';
import { Evento } from 'src/app/models/evento';
import { ActivatedRoute, Router } from '@angular/router';
import { EventoService } from 'src/app/services/eventos.service';
import { DatePipe } from '@angular/common';
import { Participante } from 'src/app/models/participante';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { AddParticipanteDialog } from './add-participante-dialog/add-participante.dialog';
import { ParticipanteEvento } from 'src/app/models/participante-evento';
import { ToastrService } from 'ngx-toastr';
import { TipoEvento } from 'src/app/models/tipo-evento';
import { TipoEventoService } from 'src/app/services/tipo-evento.service.';
import { PagosParticipanteDialog } from './pagos-participante-dialog/pagos-participante.dialog';
import { ParticipanteService } from 'src/app/services/participante.service';

@Component({
  selector: 'app-view-evento',
  templateUrl: './view-evento.component.html',
  styleUrls: ['./view-evento.component.scss']
})
export class ViewEventoComponent implements OnInit {
  keyword = "descripcion";
  isLoadingResult: boolean;

  filteredTipoEventos: Array<TipoEvento> = new Array<TipoEvento>();

  showMovimientos:boolean = true

  evento: Evento = new Evento();
  participantes: Array<ParticipanteEvento> = new Array<ParticipanteEvento>();
  listaFiltradaParticipantes: Array<ParticipanteEvento> = new Array<ParticipanteEvento>();
  participanteEvento: ParticipanteEvento = new ParticipanteEvento();

  fromDate: Date;
  toDate: Date;

  fromText: string;
  toText: string;

  constructor(
      private activatedRoute: ActivatedRoute,
      private eventoService: EventoService,
      public dialog: MatDialog,
      private router: Router,
      private toastr: ToastrService,
      private tipoEventoService: TipoEventoService) {
    this.evento.id = this.activatedRoute.snapshot.params.id;
    this.consultarTipoEventos();
    this.ConsultarEvento();
  }

  ngOnInit() {
  }


  resetMovimientosEvento(){
    this.showMovimientos = false;

    setTimeout(() => {
        this.showMovimientos = true
      }, 50);
  }

  consultarTipoEventos(){
    this.tipoEventoService.findAll().subscribe(response => {
      this.filteredTipoEventos = response;
    })
  }

  ConsultarEvento() {
    let datePipe = new DatePipe('es-MX');
    this.eventoService.consultaById(this.evento.id).subscribe(response => {
      if(!response.error) {
        this.evento = response;
        this.fromText = datePipe.transform(this.evento.fechaInicio, 'yyyy/MM/dd');
        this.toText = datePipe.transform(this.evento.fechaFin, 'yyyy/MM/dd');
        this.consultaParticipantes();
      } else {
        this.router.navigateByUrl('/momentum/eventos');
      }
    });
  }

  consultaParticipantes() {
    this.eventoService.consultaParticipantes(this.evento).subscribe(response => {
      this.participantes = response;
      this.listaFiltradaParticipantes = this.participantes;
    });
  }

  openDialog(): void {
    this.participanteEvento = new ParticipanteEvento();
    this.participanteEvento.evento = this.evento;
    let dialogRef = this.dialog.open(AddParticipanteDialog, {
      data: this.participanteEvento,
      maxHeight: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result!=undefined){
        this.participanteEvento = result;
        this.addParticipanteToEvento(this.participanteEvento);
      } else {
        this.participanteEvento = new ParticipanteEvento();
      }
    });
  }

  openDialogView(participante: ParticipanteEvento): void {
    let dialogRef = this.dialog.open(AddParticipanteDialog, {
      data: participante,
      maxHeight: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result!=undefined){
        this.participanteEvento = result;
        this.actualizarParticipanteToEvento(this.participanteEvento);
      } else {
        this.participanteEvento = new ParticipanteEvento();
      }
    });
  }

  actualizarParticipanteToEvento(participanteEvento: ParticipanteEvento) {
    this.eventoService.actualizarParticipante(participanteEvento).subscribe(response=> {
      this.consultaParticipantes();
    })
  }

  addParticipanteToEvento(participanteEvento: ParticipanteEvento) {
    this.eventoService.agregarParticipante(participanteEvento).subscribe(response=> {
      this.participantes.push(participanteEvento);
      this.consultaParticipantes();
    })
  }

  pagosParticipante(participanteEvento: ParticipanteEvento) {
    let dialogRef = this.dialog.open(PagosParticipanteDialog, {
      data: participanteEvento,
      maxHeight: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result!=undefined){
        this.participanteEvento = result;
        this.addParticipanteToEvento(this.participanteEvento);
      } else {
        this.participanteEvento = new ParticipanteEvento();
      }

      this.resetMovimientosEvento();
    });
  }

  verParticipante(participante: ParticipanteEvento) {
    this.openDialogView(participante);
  }

  eliminarParticipante(participanteEvento: ParticipanteEvento) {
    this.eventoService.eliminarParticipante(participanteEvento).subscribe(response => {
      this.toastr.info("Se ha eliminado el participante: " +  participanteEvento.participante.nombreCompleto);
      this.consultaParticipantes();
    })
  }

  onSearchChange(searchValue: string): void {
    if(searchValue.length>=3){
      this.consultarParticipanteByNombre(searchValue);
    } else if(searchValue.length==0 ){
      this.listaFiltradaParticipantes = this.participantes;
    }
  }

  consultarParticipanteByNombre(nombre: string){
    this.listaFiltradaParticipantes = this.participantes.filter(
      participanteEvento => participanteEvento.participante.nombreCompleto.toUpperCase().includes(nombre.toUpperCase())
    );
  }

  asistio(participanteEvento: ParticipanteEvento){
    this.eventoService.asistioParticipante(participanteEvento).subscribe(response => {

    })
  }

  graduado(participanteEvento: ParticipanteEvento){
    this.eventoService.graduadoParticipante(participanteEvento).subscribe(response => {

    })
  }

}


