import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Participante } from 'src/app/models/participante';
import { ParticipanteService } from 'src/app/services/participante.service';
import { ParticipanteEvento } from 'src/app/models/participante-evento';

@Component({
selector: 'dialog-add-participante',
templateUrl: 'add-participante.dialog.html',
styleUrls: ['./add-participante.dialog.scss']
})
export class AddParticipanteDialog {
  keyword = "nombreCompleto";

  costo: string = '';

  isLoadingResult: boolean;

  filteredParticipantes: Array<Participante> = new Array<Participante>();
  filteredParticipantesEnrolador: Array<Participante> = new Array<Participante>();
  participante: Participante = new Participante();
  participanteEnrolador: Participante = new Participante();
  nombreParticipanteEnrolador: string = '';

  constructor(
    public dialogRef: MatDialogRef<AddParticipanteDialog>,
    @Inject(MAT_DIALOG_DATA) public participanteEvento: ParticipanteEvento,
    private participanteService: ParticipanteService) {

    if(this.participanteEvento.participante) {
      this.participante = this.participanteEvento.participante;
      this.costo = '$ ' + this.participanteEvento.costo;

      if(this.participante.participanteEnrolador){
        this.participanteEnrolador = this.participante.participanteEnrolador;
        this.nombreParticipanteEnrolador = this.participanteEnrolador.nombreCompleto;
        this.filteredParticipantesEnrolador.push(this.participanteEnrolador);
      }
    }
  }

  filterParticipantes(value: String) {
    let filterValue = value.toUpperCase();

    if(filterValue!='') {
    this.isLoadingResult = true;

      this.participanteService.searchByName(filterValue).subscribe(response => {
        if(response == undefined) {
          this.filteredParticipantes = new Array<Participante>();
        } else {
          this.filteredParticipantes = response;
        }

        this.isLoadingResult = false;
      });
    } else {
      this.filteredParticipantes = new Array<Participante>();
    }
  }

  filterParticipantesEnrolador(value: String) {
    let filterValue = value.toUpperCase();

    if(filterValue!='') {
    this.isLoadingResult = true;

      this.participanteService.searchByName(filterValue).subscribe(response => {
        if(response == undefined) {
          this.filteredParticipantesEnrolador = new Array<Participante>();
        } else {
          this.filteredParticipantesEnrolador = response;
        }

        this.isLoadingResult = false;
      });
    } else {
      this.filteredParticipantesEnrolador = new Array<Participante>();
    }
  }

  selectParticipante(participante: Participante): string{
    if(participante!=null) {
      this.participante = participante;

      if(this.participante.participanteEnrolador){
        this.participanteEnrolador = this.participante.participanteEnrolador;
        this.nombreParticipanteEnrolador = this.participanteEnrolador.nombreCompleto;
        this.filteredParticipantesEnrolador.push(this.participanteEnrolador);
      }

      return participante.nombres + ' ' + participante.apellidoPaterno + ' ' + participante.apellidoMaterno;
    } else {
      return '';
    }
  }

  selectParticipanteEnrolador(participante: Participante): string{
    if(participante!=null) {
      this.participanteEnrolador = participante;
      return participante.nombres + ' ' + participante.apellidoPaterno + ' ' + participante.apellidoMaterno;
    } else {
      return '';
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  guardar() {

    this.participanteEvento.participante = this.participante;
    this.participanteEvento.costo = Number(this.costo.replace("$",""));

    if(this.participanteEnrolador
      && this.participanteEnrolador.id != undefined
      && this.participanteEnrolador.id != null) {
        this.participanteEvento.participante.participanteEnrolador = this.participanteEnrolador
      }

    this.dialogRef.close(this.participanteEvento);
  }

  selectEvent(item) {
    this.selectParticipante(item);
  }

  selectEventEnrolador(item) {
    this.selectParticipanteEnrolador(item);
  }

  onChangeSearch(search: string) {
    this.filterParticipantes(search);
  }

  onChangeSearchEnrolador(search: string) {
    this.filterParticipantesEnrolador(search);
  }

}
