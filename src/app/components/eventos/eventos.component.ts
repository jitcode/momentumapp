import { Component, OnInit } from '@angular/core';
import { EventoService } from 'src/app/services/eventos.service';
import { Evento } from 'src/app/models/evento';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.scss']
})
export class EventosComponent implements OnInit {

  listaEventos: Array<Evento> = new Array<Evento>();

  constructor(
    private eventoService: EventoService,
    private toastr: ToastrService,
    private router: Router) {
  }

  ngOnInit() {
  }


}
