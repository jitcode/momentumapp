import { Component, OnInit } from '@angular/core';
import { Evento } from 'src/app/models/evento';

@Component({
  selector: 'app-calendar-eventos',
  templateUrl: './calendar-eventos.component.html',
  styleUrls: ['./calendar-eventos.component.scss']
})
export class CalendarEventosComponent implements OnInit {

  meses: Array<MesCalendar>;

  mesActual: MesCalendar = new MesCalendar();

  diasCalendario: Array<Semana>;

  constructor() {

    //Pruebas
    this.mesActual.descripcion = 'Marzo'
    this.mesActual.anio = 2020;
    this.mesActual.numero = 3;
    this.mesActual.dias = [];

    this.crearCalendarioMes(this.mesActual);
  }

  ngOnInit() {
  }

  crearCalendarioMes(mes: MesCalendar){
    //debugger;
    let semana: Semana = new Semana();
    let cantidadDiasMesActual = this.getDaysOfMonth(mes.anio, mes.numero);
    let cantidadDiasMesAnterior = this.getDaysOfAfterMonth(mes.anio, mes.numero);
    let firstDayOfWeek = this.getDayOfWeek(mes.anio, mes.numero, 1);
    let lastDayOfWeek = this.getDayOfWeek(mes.anio, mes.numero, cantidadDiasMesActual);

    this.diasCalendario = new Array<Semana>();

    //Insertamos los dias del mes anterior
    for( let i = 1 ; i < firstDayOfWeek ; i++ ) {
      let dia = new Dias();

      dia.numero = cantidadDiasMesAnterior - (firstDayOfWeek - (i));
      dia.eventos = new Array<Evento>();
      dia.numeroDiaSemana = i;
      dia.esOtroMes = true;

      semana.dias.push(dia);
    }

    //Insertamos los dias del mes actual
    let diaOfWeek: number = firstDayOfWeek;

    for( let i = 1 ; i <= cantidadDiasMesActual ; i++ ) {
      let dia = new Dias();

      dia.numero = i;
      dia.eventos = new Array<Evento>();
      dia.numeroDiaSemana = diaOfWeek;
      dia.esOtroMes = false;

      semana.dias.push(dia);

      if(diaOfWeek==7){
        diaOfWeek=1;
        this.diasCalendario.push(semana);
        semana = new Semana();
      } else {
        diaOfWeek++;
      }
    }

    diaOfWeek = 1;
    //Insertamos los dias del mes siguiente
    for( let i = lastDayOfWeek + 1 ; i <= 7 ; i++ ) {
      let dia = new Dias();

      dia.numero = diaOfWeek;
      dia.eventos = new Array<Evento>();
      dia.numeroDiaSemana = i;
      dia.esOtroMes = true;

      diaOfWeek++;

      semana.dias.push(dia);
    }

    this.diasCalendario.push(semana);
  }

  getDayOfWeek(year: number, month:number, day: number): number{
    return new Date(year, month, day).getDay() -2;
  }

  getDaysOfMonth(year:number, month: number): number{
    return new Date(year,month,0).getDate();
  }

  getDaysOfAfterMonth(year:number, month: number): number{
    let mesAnterior = month == 1 ? 12 : month-1;
    let anioAnterior = month == 1 ? year - 1 : year;

    return new Date(anioAnterior,mesAnterior,0).getDate();
  }

}

export class MesCalendar {
  public numero: number;
  public anio: number;
  public descripcion: string;
  public dias: Array<DiaCalendar>;
}

export class DiaCalendar {
  public numero: number;
  public eventos: Array<Evento>;
}

export class Semana {
  public numero: number;
  public dias: Array<Dias>;

  constructor() {
    this.dias = new Array<Dias>();
  }
}

export class Dias {
  public numero: number;
  public numeroDiaSemana: number;
  public esOtroMes: boolean;
  public eventos: Array<Evento>;
}
