import { Component, OnInit, ViewChild } from '@angular/core';
import { Evento } from 'src/app/models/evento';
import { EventoService } from 'src/app/services/eventos.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { TipoEventoService } from 'src/app/services/tipo-evento.service.';
import { TipoEvento } from 'src/app/models/tipo-evento';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'card-lista-eventos',
  templateUrl: './lista-eventos.component.html',
  styleUrls: ['./lista-eventos.component.scss']
})
export class ListaEventosComponent implements OnInit {

  generacion: string = '';
  listaEventos: Array<Evento> = new Array<Evento>();
  listaEventosFiltered: Array<Evento> = new Array<Evento>();


  filteredTipoEventos: Array<TipoEvento> = new Array<TipoEvento>();
  tipoEvento: TipoEvento = {
    id: 0, descripcion: ''
  }

  displayedColumns: string[] = ['descripcion', 'fecha', 'acciones'];
  dataSource: MatTableDataSource<Evento>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    private eventoService: EventoService,
    private toastr: ToastrService,
    private router: Router,
    private tipoEventoService: TipoEventoService) {
    this.consultaEventos();
    this.consultarTipoEventos();
  }

  consultaEventos() {
    this.eventoService.findAll().subscribe(response => {
      if(!response.error) {
        this.listaEventos = response;
        this.listaEventosFiltered = this.listaEventos;
        this.crearDataSource();
      }
    })
  }

  ngOnInit() {
  }

  crearDataSource(){
    this.dataSource = new MatTableDataSource<Evento>(this.listaEventosFiltered);
    this.paginator._intl.itemsPerPageLabel='';
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ver(evento: Evento) {
    this.router.navigateByUrl('/momentum/eventos/view/'+evento.id);
  }

  eliminar(evento: Evento) {
    this.eventoService.deleteById(evento).subscribe(response => {
      this.toastr.info("Se ha eliminado correctamente el evento : " + evento.tipoEvento.descripcion);
      this.consultaEventos();
    });
  }

  editar(evento: Evento) {
    this.router.navigateByUrl('/momentum/eventos/edit/'+evento.id);
  }

  consultarTipoEventos(){
    this.tipoEventoService.findAll().subscribe(response => {
      this.filteredTipoEventos = response;
    })
  }

  onTipoEventoChange(){
    console.log(this.tipoEvento);
    if(this.tipoEvento.id == 0 && this.generacion==''){
      this.listaEventosFiltered = this.listaEventos;
    } else if (this.tipoEvento.id == 0 && this.generacion!='' ){
      this.listaEventosFiltered = this.listaEventos.filter( evento => evento.generacion == this.generacion);
    } else if (this.tipoEvento.id != 0 && this.generacion=='' ){
      this.listaEventosFiltered = this.listaEventos.filter( evento => evento.tipoEvento.id==this.tipoEvento.id)
    } else {
      this.listaEventosFiltered = this.listaEventos.filter( evento => evento.tipoEvento.id==this.tipoEvento.id && evento.generacion == this.generacion)
    }

    this.crearDataSource();
  }

  compareTiposEventos(obj1: TipoEvento, obj2: TipoEvento): boolean{
    return obj1 && obj2 && obj1.id == obj2.id;
  }

}
