import { Component, OnInit } from '@angular/core';
import { MetodoPago } from 'src/app/models/metodo-pago';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { AddMetodoPagoDialog } from './add-metodo-pago-dialog/add-metodo-pago.dialog';
import { MetodoPagoService } from 'src/app/services/metodo-pago.service';

@Component({
  selector: 'app-table-metodo-pago',
  templateUrl: './table-metodo-pago.component.html',
  styleUrls: ['./table-metodo-pago.component.scss']
})
export class TableMetodoPagoComponent implements OnInit {
  listaMetodosPagos: Array<MetodoPago> = new Array<MetodoPago>();

  constructor(
    private metodoPagoService: MetodoPagoService,
    private toastr: ToastrService,
    public dialog: MatDialog
  ) {

    this.consultarListaMetodosPagos();
  }

  ngOnInit() {
  }

  consultarListaMetodosPagos(){
    this.metodoPagoService.findAll().subscribe(response => {
      this.listaMetodosPagos = response;
    });
  }

  ver(metodoPago: MetodoPago) {
    this.openDialogView(metodoPago);
  }

  eliminar(metodoPago: MetodoPago) {
    this.metodoPagoService.eliminar(metodoPago).subscribe(response => {
      this.toastr.info("Se ha eliminado correctamente el evento : " + metodoPago.descripcion);
      this.consultarListaMetodosPagos();
    });
  }

  openDialogView(metodoPago: MetodoPago): void {

    let dialogRef = this.dialog.open(AddMetodoPagoDialog, {
      data: metodoPago,
      maxHeight: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.consultarListaMetodosPagos();
    });
  }

  openDialogAgregar(): void {
    let metodoPago: MetodoPago = new MetodoPago();

    let dialogRef = this.dialog.open(AddMetodoPagoDialog, {
      data: metodoPago,
      maxHeight: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.consultarListaMetodosPagos();
    });
  }

}
