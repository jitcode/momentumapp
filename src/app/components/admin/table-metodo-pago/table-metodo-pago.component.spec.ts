import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableMetodoPagoComponent } from './table-metodo-pago.component';

describe('TableMetodoPagoComponent', () => {
  let component: TableMetodoPagoComponent;
  let fixture: ComponentFixture<TableMetodoPagoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableMetodoPagoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableMetodoPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
