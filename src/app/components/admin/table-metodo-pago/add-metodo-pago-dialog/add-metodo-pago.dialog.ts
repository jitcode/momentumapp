import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MetodoPago } from 'src/app/models/metodo-pago';
import { MetodoPagoService } from 'src/app/services/metodo-pago.service';


@Component({
selector: 'dialog-add-metodo-pago',
templateUrl: 'add-metodo-pago.dialog.html',
styleUrls: ['./add-metodo-pago.dialog.scss']
})
export class AddMetodoPagoDialog {

  constructor(
    public dialogRef: MatDialogRef<AddMetodoPagoDialog>,
    @Inject(MAT_DIALOG_DATA) public metodoPago: MetodoPago,
    private metodoPagoService: MetodoPagoService) {
  }

  guardar() {

    if(!this.metodoPago.id) {

      this.metodoPagoService.crear(this.metodoPago).subscribe(response => {

      });
    } else {

      this.metodoPagoService.actualizar(this.metodoPago).subscribe(response=> {

      });
    }

    this.dialogRef.close(this.metodoPago);
  }

}
