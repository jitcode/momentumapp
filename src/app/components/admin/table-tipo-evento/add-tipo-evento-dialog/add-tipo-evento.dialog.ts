import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Participante } from 'src/app/models/participante';
import { ParticipanteService } from 'src/app/services/participante.service';
import { ParticipanteEvento } from 'src/app/models/participante-evento';
import { TipoEvento } from 'src/app/models/tipo-evento';
import { TipoEventoService } from 'src/app/services/tipo-evento.service.';

@Component({
selector: 'dialog-add-participante',
templateUrl: 'add-tipo-evento.dialog.html',
styleUrls: ['./add-tipo-evento.dialog.scss']
})
export class AddTipoEventoDialog {

  constructor(
    public dialogRef: MatDialogRef<AddTipoEventoDialog>,
    @Inject(MAT_DIALOG_DATA) public tipoEvento: TipoEvento,
    private tipoEventoService: TipoEventoService) {
  }

  guardar() {

    if(!this.tipoEvento.id) {

      this.tipoEventoService.crear(this.tipoEvento).subscribe(response => {

      });
    } else {

      this.tipoEventoService.actualizar(this.tipoEvento).subscribe(response=> {

      });
    }

    this.dialogRef.close(this.tipoEvento);
  }

}
