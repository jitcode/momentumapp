import { Component, OnInit } from '@angular/core';
import { TipoEvento } from 'src/app/models/tipo-evento';
import { TipoEventoService } from 'src/app/services/tipo-evento.service.';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { AddTipoEventoDialog } from './add-tipo-evento-dialog/add-tipo-evento.dialog';

@Component({
  selector: 'app-table-tipo-evento',
  templateUrl: './table-tipo-evento.component.html',
  styleUrls: ['./table-tipo-evento.component.scss']
})
export class TableTipoEventoComponent implements OnInit {

  listaTipoEventos: Array<TipoEvento> = new Array<TipoEvento>();

  constructor(
    private tipoEventoService: TipoEventoService,
    private toastr: ToastrService,
    public dialog: MatDialog
  ) {

    this.consultarListaTipoEventos();
  }

  ngOnInit() {
  }

  consultarListaTipoEventos(){
    this.tipoEventoService.findAll().subscribe(response => {
      this.listaTipoEventos = response;
    });
  }

  ver(tipoEvento: TipoEvento) {
    this.openDialogView(tipoEvento);
  }

  eliminar(tipoEvento: TipoEvento) {
    this.tipoEventoService.eliminar(tipoEvento).subscribe(response => {
      this.toastr.info("Se ha eliminado correctamente el evento : " + tipoEvento.descripcion);
      this.consultarListaTipoEventos();
    });
  }

  openDialogView(tipoEvento: TipoEvento): void {

    let dialogRef = this.dialog.open(AddTipoEventoDialog, {
      data: tipoEvento,
      maxHeight: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.consultarListaTipoEventos();
    });
  }

  openDialogAgregar(): void {
    let tipoEvento: TipoEvento = new TipoEvento();

    let dialogRef = this.dialog.open(AddTipoEventoDialog, {
      data: tipoEvento,
      maxHeight: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.consultarListaTipoEventos();
    });
  }

}
