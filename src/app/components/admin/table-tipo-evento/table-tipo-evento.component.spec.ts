import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableTipoEventoComponent } from './table-tipo-evento.component';

describe('TableTipoEventoComponent', () => {
  let component: TableTipoEventoComponent;
  let fixture: ComponentFixture<TableTipoEventoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableTipoEventoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableTipoEventoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
