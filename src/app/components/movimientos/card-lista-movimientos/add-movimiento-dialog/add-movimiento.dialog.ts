import { Component, Inject, ViewChild, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Participante } from 'src/app/models/participante';
import { MetodoPago } from 'src/app/models/metodo-pago';
import { MetodoPagoService } from 'src/app/services/metodo-pago.service';
import { Movimiento } from 'src/app/models/movimiento';
import { MovimientosService } from 'src/app/services/movimientos.service';
import { ToastrService } from 'ngx-toastr';
import { TipoMovimiento } from 'src/app/models/tipo-movimiento';
import { CatalogosService } from 'src/app/services/catalogos.service';
import { ParticipanteService } from 'src/app/services/participante.service';
import { Evento } from 'src/app/models/evento';
import { EventoService } from 'src/app/services/eventos.service';

@Component({
selector: 'add-movimiento-dialog',
templateUrl: 'add-movimiento.dialog.html',
styleUrls: ['./add-movimiento.dialog.scss']
})
export class AddMovimientoDialog implements OnInit {

  evento: Evento = new Evento();
  descripcionEvento: string = ''
  keywordEvento: string = 'descripcion';
  filteredEventos: Array<Evento> = new Array<Evento>();

  descripcion:string = '';
  monto: string = '';
  metodoPago: MetodoPago = new MetodoPago();
  listaMetodosPago: Array<MetodoPago> = new Array<MetodoPago>();
  listaMovimientos: Array<Movimiento> = new Array<Movimiento>();

  isLoadingResult: boolean;

  tipoMovimiento: TipoMovimiento = {id: 1, descripcion: 'Abono'};
  listaTiposMovimientos: Array<TipoMovimiento> = new Array<TipoMovimiento>();

  keyword: string = 'nombreCompleto';
  filteredParticipantes: Array<Participante> = new Array<Participante>();
  participante: Participante = new Participante();
  nombreParticipante: string = '';

  constructor(
    public dialogRef: MatDialogRef<AddMovimientoDialog>,
    @Inject(MAT_DIALOG_DATA) public movimiento: Movimiento,
    private metodoPagoService: MetodoPagoService,
    private movimientosService: MovimientosService,
    private catalogosService: CatalogosService,
    private participanteService: ParticipanteService,
    private eventoService: EventoService,
    private toastr: ToastrService) {

    if(this.movimiento && this.movimiento.id){
      this.descripcion = this.movimiento.descripcion;
      this.monto = '$ ' + this.movimiento.cantidad;
      this.metodoPago = this.movimiento.metodoPago;
      this.tipoMovimiento = this.movimiento.tipoMovimiento;

      if(this.movimiento.participante) {
        this.participante = this.movimiento.participante;
      }

      if(this.movimiento.evento) {
        this.evento = this.movimiento.evento;
      }

    }

    if(this.participante){
      this.nombreParticipante = this.participante.nombreCompleto;
    }

    if(this.evento){
      this.descripcionEvento = this.evento.descripcion;
    }

    this.consultarMetodoPago();
    this.consultarTiposMovimientos();
  }

  ngOnInit(){
  }

  getTotalPagado(): number{
    let sum = 0;

    sum = this.listaMovimientos.reduce((prev, cur) => {
      return prev + cur.cantidad;
    }, 0);

    return sum;
  }

  consultarTiposMovimientos() {
    this.catalogosService.findTiposMovimientos().subscribe(response => {
      this.listaTiposMovimientos = response;
    })
  }


  consultarMetodoPago() {
    this.metodoPagoService
      .findAll()
      .subscribe(response => {
      this.listaMetodosPago = response;
    })
  }

  guardar() {
    let movimiento: Movimiento = new Movimiento();

    if(this.movimiento && this.movimiento.id){
      movimiento.id = this.movimiento.id;
    }

    if(this.participante && this.participante.id)
      movimiento.participante = this.participante;
    else
      delete movimiento.participante;

    if(this.evento && this.evento.id)
      movimiento.evento = this.evento;
    else
      delete movimiento.evento;

    movimiento.cantidad = Number(this.monto.replace("$",""));
    movimiento.metodoPago = this.metodoPago;
    movimiento.tipoMovimiento = this.tipoMovimiento;
    movimiento.descripcion = this.descripcion;

    this.movimientosService
      .crearParticipanteMovimiento(movimiento)
      .subscribe(response => {
        this.toastr.info("Se ha guardado el pago correctamente.");
        this.limpiarCampos();
     })

    this.dialogRef.close();

  }

  limpiarCampos(){
    this.metodoPago = new MetodoPago();
    this.monto = '';
  }

  compareIds(o1: any, o2: any): boolean {
    return o1 && o2 && o1.id === o2.id;
  }

  filterParticipantes(value: String) {
    let filterValue = value.toUpperCase();

    if(filterValue!='') {
    this.isLoadingResult = true;

      this.participanteService.searchByName(filterValue).subscribe(response => {
        if(response == undefined) {
          this.filteredParticipantes = new Array<Participante>();
        } else {
          this.filteredParticipantes = response;
        }

        this.isLoadingResult = false;
      });
    } else {
      this.filteredParticipantes = new Array<Participante>();
    }
  }

  selectParticipante(participante: Participante): string{
    if(participante!=null) {
      this.participante = participante;
      return participante.nombres + ' ' + participante.apellidoPaterno + ' ' + participante.apellidoMaterno;
    } else {
      return '';
    }
  }

  selectEvent(item) {
    this.selectParticipante(item);
  }

  onChangeSearch(search: string) {
    this.filterParticipantes(search);
  }

  filterEventos(value: String) {
    let filterValue = value.toUpperCase();

    if(filterValue!='') {
    this.isLoadingResult = true;

      this.eventoService.searchByDescripcion(filterValue).subscribe(response => {
        if(response == undefined) {
          this.filteredEventos = new Array<Evento>();
        } else {
          this.filteredEventos = response;
        }

        this.isLoadingResult = false;
      });
    } else {
      this.filteredEventos = new Array<Evento>();
    }
  }

  selectEvento(evento: Evento): string{
    if(evento!=null) {
      this.evento = evento;
      return this.evento.descripcion;
    } else {
      return '';
    }
  }

}
