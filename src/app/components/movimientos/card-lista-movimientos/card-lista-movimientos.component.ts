import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Movimiento } from 'src/app/models/movimiento';
import { MovimientosService } from 'src/app/services/movimientos.service';
import { ToastrService } from 'ngx-toastr';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Evento } from 'src/app/models/evento';
import { EventoService } from 'src/app/services/eventos.service';
import { AddMovimientoDialog } from './add-movimiento-dialog/add-movimiento.dialog';
import { Participante } from 'src/app/models/participante';
import { ParticipanteService } from 'src/app/services/participante.service';
import { NgbCalendar, NgbDateParserFormatter, NgbDate } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'card-lista-movimientos',
  templateUrl: './card-lista-movimientos.component.html',
  styleUrls: ['./card-lista-movimientos.component.scss']
})
export class CardListaMovimientosComponent implements OnInit {

  //NGBDatePicker
  hoveredDate: NgbDate | null = null;
  fromDate: NgbDate | null;
  toDate: NgbDate | null;

  //Autocomplete Eventos
  evento: Evento = new Evento();
  descripcionEvento: string = ''
  keywordEvento: string = 'descripcion';
  filteredEventos: Array<Evento> = new Array<Evento>();
  isLoadingResult: boolean = false;

  //Autocomplete Participante
  keyword: string = 'nombreCompleto';
  filteredParticipantes: Array<Participante> = new Array<Participante>();
  participante: Participante = new Participante();
  nombreParticipante: string = '';

  listaMovimientos: Array<Movimiento> = new Array<Movimiento>();
  listaMovimientosFiltered: Array<Movimiento> = new Array<Movimiento>();

  displayedColumns: string[] = ['noMovimiento', 'descripcion', 'fecha', 'cantidad', 'metodoPago','acciones'];
  dataSource: MatTableDataSource<Movimiento>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private movimientosService: MovimientosService,
    private toastr: ToastrService,
    private eventoService: EventoService,
    private participanteService: ParticipanteService,
    private calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter
  ) {
    this.consultarMovimientos();
  }

  ngOnInit() {
  }

  agregar() {
    let dialogRef = this.dialog.open(AddMovimientoDialog, {
      data: new Movimiento(),
      maxHeight: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.consultarMovimientos();
    });
  }

  initDataSource(){
    this.dataSource = new MatTableDataSource<Movimiento>(this.listaMovimientosFiltered);
    this.dataSource.sort = this.sort;
    this.paginator._intl.itemsPerPageLabel = "";
    this.dataSource.paginator = this.paginator;
  }

  verPago(movimiento: Movimiento){
    let dialogRef = this.dialog.open(AddMovimientoDialog, {
      data: movimiento,
      maxHeight: '90vh'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.consultarMovimientos();
    });
  }

  consultarMovimientos() {
    this.movimientosService.findAll().subscribe(response => {
      this.listaMovimientos = response;
      this.filtrarMovimientos();
    })
  }

  filtrarMovimientos() {
    this.listaMovimientosFiltered = this.listaMovimientos;

    //Filtramos el evento
    if(this.evento && this.evento.id){
      this.listaMovimientosFiltered = this.listaMovimientosFiltered.filter(movimiento => movimiento.evento && movimiento.evento.id == this.evento.id);
    }

    //Filtramos participante
    if(this.participante && this.participante.id){
      this.listaMovimientosFiltered = this.listaMovimientosFiltered.filter(movimiento => movimiento.participante && movimiento.participante.id == this.participante.id);
    }

    //Filtramos por fecha
    if(this.fromDate && this.toDate){
      let desdeDate: Date = new Date(this.fromDate.year, this.fromDate.month-1, this.fromDate.day);
      let hastaDate: Date = new Date(this.toDate.year, this.toDate.month-1, this.toDate.day);

      this.listaMovimientosFiltered = this.listaMovimientosFiltered.filter(movimiento =>{
        let movimientoFecha = new Date(movimiento.fecha);
       return movimientoFecha.getTime() >= desdeDate.getTime() && movimientoFecha.getTime() <= hastaDate.getTime()
      });
    }

    this.initDataSource();
  }

  eliminarPago(movimiento: Movimiento){
    this.movimientosService.eliminar(movimiento).subscribe(response => {
      this.toastr.info("Se ha eliminado el movimiento.");
      this.consultarMovimientos();
    })
  }

  padWithZeros(num:number, size:number): string {
    let s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
  }

  filterEventos(value: String) {
    let filterValue = value.toUpperCase();

    if(filterValue!='') {
    this.isLoadingResult = true;

      this.eventoService.searchByDescripcion(filterValue).subscribe(response => {
        if(response == undefined) {
          this.filteredEventos = new Array<Evento>();
        } else {
          this.filteredEventos = response;
        }

        this.isLoadingResult = false;
      });
    } else {
      this.filteredEventos = new Array<Evento>();
    }
  }

  selectEvento(evento: Evento): string{
    if(evento!=null) {
      this.evento = evento;
      this.filtrarMovimientos();
      return this.evento.descripcion;
    } else {
      return '';
    }
  }

  filterParticipantes(value: String) {
    let filterValue = value.toUpperCase();

    if(filterValue!='') {
    this.isLoadingResult = true;

      this.participanteService.searchByName(filterValue).subscribe(response => {
        if(response == undefined) {
          this.filteredParticipantes = new Array<Participante>();
        } else {
          this.filteredParticipantes = response;
        }

        this.isLoadingResult = false;
      });
    } else {
      this.filteredParticipantes = new Array<Participante>();
    }
  }

  selectParticipante(participante: Participante): string{
    if(participante!=null) {
      this.participante = participante;
      this.filtrarMovimientos();
      return participante.nombres + ' ' + participante.apellidoPaterno + ' ' + participante.apellidoMaterno;
    } else {
      return '';
    }
  }

  onEventoCleared() {
    this.evento = new Evento();
    this.filtrarMovimientos();
  }

  onParticipanteCleared() {
    this.participante = new Participante();
    this.filtrarMovimientos();
  }

  getTotalPagado(): number{
    let sum = 0;

    sum = this.listaMovimientosFiltered.reduce((prev, cur) => {
      return prev + (cur.tipoMovimiento.id == 1? cur.cantidad: cur.cantidad * -1);
    }, 0);

    return sum;
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }

    this.filtrarMovimientos();
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return this.toDate && date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || (this.toDate && date.equals(this.toDate)) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate | null, input: string): NgbDate | null {
    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

}
